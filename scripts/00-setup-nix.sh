#!/usr/bin/env bash
set -xe

sh <(curl -L https://nixos.org/nix/install) --no-daemon
addgroup --system nixbld \
  && adduser gitpod nixbld \
  && for i in $(seq 1 30); do useradd -ms /bin/bash nixbld$i &&  adduser nixbld$i nixbld; done \
  && mkdir -m 0755 /nix && chown gitpod /nix \
  && mkdir -p /etc/nix \
  && echo 'sandbox = false' > /etc/nix/nix.conf \
  && echo 'experimental-features = nix-command flakes' >> /etc/nix/nix.conf

 . ~/.nix-profile/etc/profile.d/nix.sh \
  && nix-env -iA cachix -f https://cachix.org/api/v1/install \
  && nix-channel --add https://nixos.org/channels/nixpkgs-unstable unstable \
  && nix-channel --add https://github.com/nix-community/home-manager/archive/master.tar.gz home-manager \
  && nix-channel --update \
  && nix-shell '<home-manager>' -A install

. ~/.nix-profile/etc/profile.d/hm-session-vars.sh \
  && cachix use cachix \
  && echo ". ~/.nix-profile/etc/profile.d/nix.sh" >> ~/.bashrc \
  && echo ". ~/.nix-profile/etc/profile.d/hm-session-vars.sh" >> ~/.bashrc \
  && mkdir -p ~/.config/nix \
  && echo 'sandbox = false' >> ~/.config/nix/nix.conf \
  && echo 'experimental-features = nix-command flakes' >> ~/.config/nix/nix.conf \
  && home-manager switch -b backup --flake ~/.dotfiles#gitpod --extra-experimental-features nix-command --extra-experimental-features flakes

