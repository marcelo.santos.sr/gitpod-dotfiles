{
  description = "gitpod dotfiles flake";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-unstable";
    maxhero-flake.url = "github:themaxhero/.dotfiles";
    maxhero-flake.inputs.nixpkgs.follows = "nixpkgs";
    home-manager.url = "github:nix-community/home-manager";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, nixpkgs, home-manager, maxhero-flake }@attrs: rec {
    formatter.x86_64-linux = nixpkgs.legacyPackages.x86_64-linux.nixfmt;
    gitpodHome = maxhero-flake.mkHome {
      username = "gitpod";
      homeDirectory = "/home/gitpod";
      enableDevelopment = true;
      extraPackages = with nixpkgs.legacyPackages.x86_64-linux; [
        openshift
        ocm
        asdf-vm
      ];
      extraModules = [
        ({ ... }: {
          programs.zsh.initExtra = ''
            . ~/.nix-profile/etc/profile.d/nix.sh
            . ~/.nix-profile/etc/profile.d/hm-session-vars.sh
          '';
          programs.bash.bashrcExtra = ''
            . ~/.nix-profile/etc/profile.d/nix.sh
            . ~/.nix-profile/etc/profile.d/hm-session-vars.sh
          '';
          programs.git = {
            enable = true;
            userName = "Marcelo Amancio de Lima Santos";
            userEmail = "marcelo.santos@salesrabbit.com";
            extraConfig = {
              rerere.enabled = true;
              pull.rebase = true;
              tag.gpgsign = true;
              init.defaultBranch = "master";
              core = {
                excludefile = "$NIXOS_CONFIG_DIR/scripts/gitignore";
                editor = "vim";
              };
            };
          };
          programs.home-manager.enable = true;
        })
      ];
    };

    homeConfigurations.gitpod = home-manager.lib.homeManagerConfiguration {
      pkgs = nixpkgs.legacyPackages.x86_64-linux;
      modules = [
        { nixpkgs.config.allowUnfree = true; }
        gitpodHome
      ];
      extraSpecialArgs = attrs // { self = maxhero-flake; };
    };
  };
}
