#!/usr/bin/env bash

TMPDIR=$(mktemp -d)

CURRENT=$PWD

cd $TMPDIR

for script in ~/.dotfiles/scripts/*; do
  bash "$script"
done

cd $CURRENT

. ~/.nix-profile/etc/profile.d/nix.sh
. ~/.nix-profile/etc/profile.d/hm-session-vars.sh
direnv hook bash >> ~/.bashrc
direnv allow .

rm -rf $TMPDIR
